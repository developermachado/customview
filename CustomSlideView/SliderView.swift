//
//  SliderView.swift
//  CustomSlideView
//
//  Created by Carlos Henrique Machado on 18/02/20.
//  Copyright © 2020 Carlos Henrique Machado. All rights reserved.
//

import Foundation
import UIKit

class SliderView: UIView {
    
    private var parent = UIView()
    private var container = UIView()
    private let borderContainer = UIView()
    private var values: [ColorDataModel] = []
    private let thumbView = UIView()
    private var initialCenter = CGPoint()
    private let marginValue: CGFloat = 20.0
    private let subMarginValue: CGFloat = 6.0
    private let defaultMultiplier: CGFloat = 1.0
    
    func create(data values: [ColorDataModel]) {
        self.parent = superview ?? UIView()
        self.values = values
        createContainer()
        colorViewSetup()
        createThumb()
    }
    
    private func createContainer() {
        borderContainer.backgroundColor = .none
        borderContainer.layer.cornerRadius = parent.layer.cornerRadius
        borderContainer.layer.borderWidth = 3
        borderContainer.layer.borderColor = UIColor.lightGray.cgColor
        borderContainer.translatesAutoresizingMaskIntoConstraints = false
        parent.addSubview(borderContainer)
        
        NSLayoutConstraint.activate([
            borderContainer.topAnchor.constraint(equalTo: parent.topAnchor, constant: 10),
            NSLayoutConstraint(item: borderContainer, attribute: .leading, relatedBy: .equal, toItem: parent, attribute: .trailing, multiplier: 0.05, constant: 0),
            NSLayoutConstraint(item: borderContainer, attribute: .trailing, relatedBy: .equal, toItem: parent, attribute: .trailing, multiplier: 0.95, constant: 0),
            borderContainer.bottomAnchor.constraint(equalTo: parent.bottomAnchor, constant: -30),
        ])
        
        container.backgroundColor = .none
        container.layer.cornerRadius = parent.layer.cornerRadius
        container.layer.borderWidth = 1
        container.layer.borderColor = UIColor.lightGray.cgColor
        container.clipsToBounds = true
        container.translatesAutoresizingMaskIntoConstraints = false
        borderContainer.addSubview(container)
        
        NSLayoutConstraint.activate([
            container.topAnchor.constraint(equalTo: borderContainer.topAnchor, constant: subMarginValue),
            container.leadingAnchor.constraint(equalTo: borderContainer.leadingAnchor, constant: subMarginValue),
            container.bottomAnchor.constraint(equalTo: borderContainer.bottomAnchor, constant: -subMarginValue),
            container.trailingAnchor.constraint(equalTo: borderContainer.trailingAnchor, constant: -subMarginValue)
        ])
    }
    
    private func createThumb(_ multiplier: CGFloat = 1.0) {
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        thumbView.addGestureRecognizer(gesture)
        thumbView.backgroundColor = .orange
        thumbView.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(thumbView)
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: thumbView,
                               attribute: .height,
                               relatedBy: .equal,
                               toItem: container,
                               attribute: .height,
                               multiplier: 2.0,
                               constant: 0),
            
            NSLayoutConstraint(item: thumbView,
                               attribute: .width,
                               relatedBy: .equal,
                               toItem: container,
                               attribute: .width,
                               multiplier: 0.05,
                               constant: 0),
            
            NSLayoutConstraint(item: thumbView,
                               attribute: .top,
                               relatedBy: .equal,
                               toItem: container,
                               attribute: .top,
                               multiplier: 1,
                               constant: 0),
            
            NSLayoutConstraint(item: thumbView,
                               attribute: .centerX,
                               relatedBy: .equal,
                               toItem: container,
                               attribute: .trailing,
                               multiplier: multiplier,
                               constant: 0)
        ])
    }
    
    @objc func handlePan(_ gesture: UIPanGestureRecognizer) {
        guard let piece = gesture.view else { return }
        let translation = gesture.translation(in: piece.superview)
        
        if gesture.state == .began {
            self.initialCenter = piece.center
        }
        
        if gesture.state == .ended {
            let proportionalPercentage = (piece.center.x/container.frame.maxX)
            thumbView.removeFromSuperview()
            createThumb(proportionalPercentage)
        }
        
        if gesture.state != .cancelled {
            let newCenter = CGPoint(x: initialCenter.x + translation.x, y: initialCenter.y)
            piece.center = newCenter
        } else {
            piece.center = initialCenter
        }
    }
    
    private func colorViewSetup() {
        let colorView = ColorView()
        container.addSubview(colorView)
        colorView.create(values: values)
    }
}
