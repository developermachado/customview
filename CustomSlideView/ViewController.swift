//
//  ViewController.swift
//  CustomSlideView
//
//  Created by Carlos Henrique Machado on 18/02/20.
//  Copyright © 2020 Carlos Henrique Machado. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let mock = Mock()
    let sliderContainer = UIView()
    let colorContainer = UIView()
    let radius: CGFloat = 10.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        sliderContainerSetup()
        colorContainerSetup()
    }
    
    func sliderContainerSetup() {
        view.addSubview(sliderContainer)
        sliderContainer.backgroundColor = .none
        sliderContainer.layer.cornerRadius = radius
        sliderContainer.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            sliderContainer.topAnchor.constraint(equalTo: view.topAnchor, constant: 200),
            sliderContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            sliderContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            sliderContainer.heightAnchor.constraint(equalToConstant: 100)
        ])
        sliderSetup()
    }
    
    func colorContainerSetup() {
        let marginConstant: CGFloat = 20.0
        view.addSubview(colorContainer)
        colorContainer.backgroundColor = .none
        colorContainer.layer.cornerRadius = radius
        colorContainer.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            colorContainer.topAnchor.constraint(equalTo: sliderContainer.bottomAnchor, constant: 50),
            colorContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: marginConstant),
            colorContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -marginConstant),
            colorContainer.heightAnchor.constraint(equalToConstant: 50)
        ])
        colorViewSetup()
    }
    
    func sliderSetup() {
        let slider = SliderView()
        sliderContainer.addSubview(slider)
        slider.create(data: mock.getSliderValues())
    }
    
    func colorViewSetup() {
        let colorView = ColorView()
        colorContainer.addSubview(colorView)
        colorView.create(values: mock.getColorValues())
    }
}

