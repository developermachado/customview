//
//  ColorView.swift
//  CustomSlideView
//
//  Created by Carlos Henrique Machado on 18/02/20.
//  Copyright © 2020 Carlos Henrique Machado. All rights reserved.
//

import Foundation
import UIKit

class ColorView: UIView {
    
    private var values: [ColorDataModel] = []
    private var parent = UIView()
    private var container = UIView()
    
    func create(values: [ColorDataModel]) {
        self.values = values
        self.parent = superview ?? UIView()
        createContainer()
        createColorView()
    }
    
    private func createContainer() {
        container.backgroundColor = .none
        container.layer.cornerRadius = parent.layer.cornerRadius
        container.clipsToBounds = true
        container.translatesAutoresizingMaskIntoConstraints = false
        parent.addSubview(container)
        
        NSLayoutConstraint.activate([
            container.topAnchor.constraint(equalTo: parent.topAnchor),
            container.leadingAnchor.constraint(equalTo: parent.leadingAnchor),
            container.trailingAnchor.constraint(equalTo: parent.trailingAnchor),
            container.bottomAnchor.constraint(equalTo: parent.bottomAnchor)
        ])
    }
    
    private func createColorView() {
        var colorViews = [UIView]()
        
        for value in values {
            let colorView = UIView()
            let percentage = (value.value / (values.reduce(0) { $0 + $1.value }))
            
            colorView.backgroundColor = value.color
            colorView.translatesAutoresizingMaskIntoConstraints = false
            container.addSubview(colorView)
            
            var leadConstraint = NSLayoutConstraint()

            if let view = colorViews.last {
                leadConstraint = colorView.leadingAnchor.constraint(equalTo: view.trailingAnchor)
            } else {
                leadConstraint = colorView.leadingAnchor.constraint(equalTo: container.leadingAnchor)
            }
            
            let dynamicWidthConstraint = NSLayoutConstraint(item: colorView,
                                                   attribute: .width,
                                                   relatedBy: .lessThanOrEqual,
                                                   toItem: container,
                                                   attribute: .width,
                                                   multiplier: CGFloat(percentage),
                                                   constant: 0)
            
            NSLayoutConstraint.activate([
                colorView.topAnchor.constraint(equalTo: container.topAnchor),
                colorView.bottomAnchor.constraint(equalTo: container.bottomAnchor),
                leadConstraint,
                dynamicWidthConstraint
            ])
            
            colorViews.append(colorView)
        }
    }
}
