//
//  Extensions.swift
//  CustomSlideView
//
//  Created by Carlos Henrique Machado on 18/02/20.
//  Copyright © 2020 Carlos Henrique Machado. All rights reserved.
//

import Foundation

import UIKit

extension UIView {
    
    func roundCorner(_ radius: CGFloat, corner: Corner) {
        self.layer.cornerRadius = radius
        switch corner {
        case .right:
            self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        case .left:
            self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
            
        }
    }
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(item: self.firstItem!,
                                            attribute: self.firstAttribute,
                                            relatedBy: self.relation,
                                            toItem: self.secondItem,
                                            attribute: self.secondAttribute,
                                            multiplier: multiplier,
                                            constant: self.constant)
        constraint.isActive = true
        return constraint
    }
}

