//
//  Models.swift
//  CustomSlideView
//
//  Created by Carlos Henrique Machado on 18/02/20.
//  Copyright © 2020 Carlos Henrique Machado. All rights reserved.
//

import UIKit

enum Corner: Int {
    case left
    case right
}

enum Color: Int {
    case red
    case blue
    case green
}

struct ColorDataModel {
    var value: Double
    var color: UIColor
    
    init(value: Double, color: UIColor) {
        self.value = value
        self.color = color
    }
}
