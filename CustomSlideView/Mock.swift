//
//  Mock.swift
//  CustomSlideView
//
//  Created by Carlos Henrique Machado on 18/02/20.
//  Copyright © 2020 Carlos Henrique Machado. All rights reserved.
//

import Foundation

class Mock {
    private var colorData: [ColorDataModel] = []
    
    func getColorValues() -> [ColorDataModel] {
        colorData = [ColorDataModel(value: 37, color: .red),
                     ColorDataModel(value: 109, color: .blue),
                     ColorDataModel(value: 300, color: .purple),
                     ColorDataModel(value: 200, color: .black),
                     ColorDataModel(value: 79, color: .green)]
        
     return colorData
    }
    
    func getSliderValues() -> [ColorDataModel] {
        colorData = [ColorDataModel(value: 100, color: .lightGray),
                     ColorDataModel(value: 900, color: .white)]
        return colorData
    }
}
